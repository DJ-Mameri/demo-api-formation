API Demo Symfony
==============================

First Installation
------------------

    * Command line: "git clone https://DJ-Mameri@bitbucket.org/DJ-Mameri/demo-api-formation.git"

    * Command line: "cd demo-api-formation"

    * Command line: "cp .env.dist .env"

    * Edit file ".env", set database access parameters and set APP_ENV

    * Command line: "composer install"

    * Command line: "php bin/console doctrine:database:create"

    * Command line: "php bin/console doctrine:migrations:migrate"

    * Command line: "php bin/console doctrine:fixtures:load"

    * Command line: "php bin/phpunit"


